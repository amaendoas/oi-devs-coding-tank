/*
A seguir, calcule o menor número de notas possíveis (cédulas) no qual o valor pode ser decomposto.
As notas consideradas são de 200, 100, 50, 20, 10, 5, 2 e 1. 
A seguir mostre o valor lido e a relação de notas necessárias.
A quantidade mínima de notas de cada tipo necessárias, conforme o exemplo fornecido.

EX: R$ 503,00 é:

503/200 => 2,xx => 103
103/100 => 1,xx => 3
3/50 => 0,xx => 3
3/20 => 0,xx => 3
3/10 => 0,xx => 3
3/5 => 0,xx => 3
3/2 => 1,xx => 1
1/1 => 1
*/

let cedulas = [200, 100, 50, 20, 10, 5, 2, 1];
let restante = 503;
let resultado = {};

for (let index = 0; index < cedulas.length; index++) {
  let quantidade = Math.floor(restante / cedulas[index]);
  restante = restante % cedulas[index];
  resultado[cedulas[index]] = quantidade;
}

console.log(resultado);