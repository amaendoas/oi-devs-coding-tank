let notas = [
  {
    valor: 8.5,
    peso: 0.25
  },
  {
    valor: 9.9,
    peso: 0.95
  },
  {
    valor: 5,
    peso: 0.5
  },
  {
    valor: 2,
    peso: 0.25
  },
  {
    valor: 10,
    peso: 1
  },
  {
    valor: 5.5,
    peso: 0.25
  },
  {
    valor: 6,
    peso: 0.2
  },
  {
    valor: 9,
    peso: 0.75
  }
]

let aritmetica = 0;
let ponderada = 0;
let peso = 0;
let maior = notas[0].valor;
let menor = notas[0].valor;

for (let index = 0; index < notas.length; index++) {
  const nota = notas[index];
  aritmetica += nota.valor; // aritmetica = aritmetica + nota.valor;
  ponderada += nota.valor * nota.peso;
  peso += nota.peso;

  if (nota.valor > maior) {
    maior = nota.valor;
  }
  if (nota.valor < menor) {
    menor = nota.valor;
  }
}

aritmetica /= notas.length;
ponderada /= peso;

console.log(`a média aritmética é ${aritmetica.toFixed(2)} com 2 casas`);
console.log(`a média aritmética é ${aritmetica}`);
console.log(`a média ponderada é ${ponderada.toFixed(2)}`);
console.log(`o maior valor é ${maior}`);
console.log(`o menor valor é ${menor}`);