let caixinha = 10 % 2
let e_par = caixinha == 0
let e_impar = !e_par
let texto = "hello world";

// outros jeitos de concatenar strings
console.log("o valor da caixinha é: " + caixinha)
console.log('o valor da caixinha é: ' + caixinha)
console.log(`o valor da caixinha é: ${caixinha} e é par? ${e_par}`)


// console.log(caixinha)
// console.log(e_par)
// console.log(e_impar)

caixinha = 11 % 2;
const caixinha_constante = caixinha;

// console.log("A" == "B") // false
// console.log("A" != "B") // true
// console.log("10" == 10) // true
// console.log("10" === 10) // false
// console.log(typeof "A" == typeof "B") // true