let numero = 10;
let numero2 = 10;
// numero += 5; // numero = numero + 5
// numero++;
// numero+=1;
// numero = numero + 1;

/*
a) True and True
b) False and True
c) 1 == 1 and 2 == 1
d) "test" == "test"
e) 1 == 1 or 2 != 1
f) True and 1 == 1
g) False and 0 != 0
h) True or 1 == 1
i) "test" == "testing"
j) 1 != 0 and 2 == 1
k) "test" != "testing"
l) "test" == 1
m) not (True and False)
n) not (1 == 1 and 0 != 1)
*/

console.log('a)', true && true)
console.log('b)', false && true)
console.log('c)', 1 == 1 && 2 == 1)
console.log('d)', "test" == "test")
console.log('e)', 1 == 1 || 2 != 1)
console.log('f)', true && 1 == 1)
console.log('g)', false && 0 != 0)
console.log('h)', true || 1 == 1)
console.log('i)', "test" == "testing")
console.log('j)', 1 != 0 && 2 == 1)
console.log('k)', "test" != "testing")
console.log('l)', "test" == 1)
console.log('m)', !(true && false))
console.log('n)', !(1 == 1 && 0 != 1))