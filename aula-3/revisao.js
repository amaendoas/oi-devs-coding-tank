let notas_estrutura = {
  ana: [9, 2, 10],
  bruna: [5, 5, 5],
  carol: [0, 10, 10]
};

// console.log("notas_estrutura = ", notas_estrutura.ana)
// console.log("notas_estrutura = ", notas_estrutura["ana"])

let aluno = {
  nome: "Ana Carolina",
  notas: [9, 2, 10, 3456, 23, 1234, 123, 123, 235, 456, 123, 235, 456, 2456, 123, 12, 124, 235, 123, 1]
}

let meio_array = Math.floor((aluno.notas.length - 1) / 2);

console.log("meio_array = ", meio_array)
console.log("aluno = ", aluno.nome)
console.log("aluno = ", aluno.notas[meio_array])

let alunos = [
  {
    nome: "Ana Carolina",
    notas: [9, 2, 10]
  },
  {
    nome: "Bruna",
    notas: [9, 2, 10]
  },
  {
    nome: "Carol",
    notas: [0, 10, 10]
  },
]

// console.log("alunos = ", alunos[0].nome)
// console.log("alunos = ", alunos[0].notas)